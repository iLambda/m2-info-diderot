# Inductive types

## Simple inductive types

We use the Martin-Löf scheme :

* 1 formation rule
  $$
  \frac{}{\vdash \mathbb N : \mathrm{Type}}
  $$

* 2 introduction rules :
  $$
  \frac{}{\vdash 0 : \mathbb N} \qquad \frac{\vdash n : \mathbb N}{\vdash S(n) : \mathbb N}
  $$

* 1 elimination rule, that represents the fact that $\mathbb N$ is the least type closed by $0$ and $S$.
