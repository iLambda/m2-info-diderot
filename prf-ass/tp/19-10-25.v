(* Require Import Lia Omega.
Require Import ZArith.
Require Import ZArithRing.
Require Import Ring.

Open Scope Z_scope.

(* Dependant pair type *)
Record DepPair (A: Set) (B: A -> Set) := mkPair
{
    fst: A;
    snd: B fst;
}.

(* The eta rule *)
Lemma eta_dep : forall (A: Set) (B: A -> Set) (u: DepPair A B), u = {| fst := fst A B u ; snd := snd A B u |}.
Proof.
    intros.
    destruct u.
    trivial.
Qed.

(* Defuine Q *)
Record Rat : Set := mkRat 
{  
    n: Z; 
    d: Z;
}.

Definition equiv_rat (r1 r2: Rat) := r1.(n) * r2.(d) = r1.(d) * r2.(n).
Definition add_rat (r1 r2: Rat) := {| n := r1.(n) * r2.(d) + r2.(n) * r1.(d); d := r1.(d) * r2.(d) |}.

Lemma add_ok_equiv : forall (r r1 r2: Rat), equiv_rat r1 r2 <-> equiv_rat (add_rat r r1) (add_rat r r2).
Proof.
    intros.
    split.
    - unfold add_rat. 
      unfold equiv_rat.
      intros.
      simpl.
      ring_simplify.
      replace (d r1 * d r ^ 2 * n r2) with (d r1 * n r2 * d r ^ 2).
      rewrite <- H.
      simpl.
Admitted.
*)
(* Record monoid *)
Record monoid (carrier: Type) := { 
    unit : carrier;
    op : carrier -> carrier -> carrier;
    left_unit : forall x, op x unit = x;
    right_unit: forall x, op unit x = x; 
    assoc: forall x y z, op (op x y) z = op x (op y z);
}.
 

Require Import Lia Omega.
Require Import ZArith.

Definition nat_add : monoid nat.
Proof.
    refine 
        {|
            unit := 0;
            op := fun (x y : nat) => x + y;
            left_unit :=  _;
            right_unit := _;
            assoc := _;
        |} ; intros ; omega.  
Qed.

Definition nat_mul : monoid nat.
Proof.
    refine 
        {|
            unit := 1;
            op := fun (x y : nat) => x * y;
            left_unit :=  _;
            right_unit := _;
            assoc := _;
        |} ; intros; lia.  
Qed.

Fixpoint mpow (A: Type) (m: monoid A) (a: A) (n: nat) := 
    match n with 
        | 0 => m.(unit A)
        | 1 => a
        | S n' => m.(op A) a (mpow A m a n')
    end.

Lemma l2 (A: Type) (m: monoid A) : forall (n: nat), mpow A m m.(unit A) n = m.(unit A).
Proof.
    induction n.
    + cbn. trivial.
    + simpl. rewrite m.(right_unit A). rewrite IHn. destruct n; easy.
Qed.


      
      
      
      
      