Require Import List.
Import ListNotations.


(* Exercice 1 *)
Theorem cons_inj : forall (A: Type) (x1 x2: A) (l1 l2 : list A), 
                          cons x1 l1 = cons x2 l2 -> x1=x2 /\ l1=l2.
Proof.
    intros.
    split.
    * assert (hd x1 (x1::l1) = hd x2 (x2::l2)).
        rewrite H.
        cbv.
        trivial.
      cbv in H0.
      trivial.
    * assert (tl (x1::l1) = tl (x2::l2)).
        rewrite H.
        cbv.
        trivial.
      cbv in H0.
      trivial.
Qed.

Theorem cons_disc : forall (A: Type) (x: A) (l : list A), nil <> cons x l.
Proof.
    intros.
    intro.
    apply f_equal with (f:= fun x => match x with [] => True | _ => False end) in H.
    rewrite <- H.
    trivial.
Qed.

(* Exercice 2 *)

(* Exercice 3 *)
Fixpoint leq (n p: nat) {struct n} : bool :=
    match n, p with
        | O, _ => true
        | S _, 0 => false
        | S n', S p' => leq n' p'
    end.    

(* Exercice 4 *)