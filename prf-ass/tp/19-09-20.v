(*
 *  Ex 3
 *)
Check Prop.               (* Impredicative *)
Check Type.               (* Predicative *)
Check Prop -> Prop.       (* Impredicative *)
Check forall (P:Prop), P. (* Impredicative *)
Check forall (P:Type), P. (* Predicative *)

(*
 *  Ex 4
 *)
(* Equality *)
Definition equal (A: Prop) (x y: A) := forall (P: A -> Prop), P x -> P y.

Theorem equal_refl : forall (A: Prop) (x: A), equal A x x.
Proof.
  intros.
  intro. auto.
Qed.

Theorem equal_sym : forall (A: Prop) (x y: A), equal A x y <-> equal A y x.
Proof.
  intros.
  split; intro; intro; apply H; auto.
Qed.

Theorem equal_trans : forall (A: Prop) (x y z: A), equal A x y /\ equal A y z -> equal A x z.
Proof.
  intros.
  destruct H.
  intro. apply H0. apply H.
Qed.

Theorem equal_cong : forall (A B: Prop) (f: A -> B) (x y: A), equal A x y -> equal B (f x) (f y).
Proof.
  intros.
  intro. intro. red in H.
  specialize (H (fun (z: A) => equal B (f x) (f z))). 
  simpl in H.
  assert (equal B (f x) (f x)).
    apply equal_refl.
  apply H in H1.
  red in H1. specialize (H1 (P)). apply H1 in H0. auto. 
Qed.


(* Booleans *)
Definition bool := forall Q: Prop, Q->Q->Q.
Definition true (Q: Prop) (t f : Q) := t.
Definition false (Q: Prop) (t f : Q) := f.
Definition ifthenelse (Q: Prop) (c t f : bool) := c _ t f.
Definition and (Q:Prop) (x y : bool) := 
  ifthenelse Q x y false.

Theorem and_correct_true_true : (forall Q: Prop, and Q true true = true).
Proof.
  auto.
Qed.

Theorem and_correct_true_false : (forall Q: Prop, and Q true false = false).
Proof.
  auto.
Qed.

Theorem and_correct_false_true : (forall Q: Prop, and Q false true = false).
Proof.
  auto.
Qed.

Theorem and_correct_false_false : (forall Q: Prop, and Q false false = false).
Proof.
  auto.
Qed.

(* Natural numbers *)
Definition cnat := forall Q: Prop, Q->(Q->Q)->Q.
Definition zero (Q: Prop) (x: Q) (f: Q->Q) := x.
Definition succ (Q: Prop) (n: cnat) := 
  fun (x: Q) (f: Q->Q) => 
    f (n Q x f).

Definition add (Q: Prop) (n m: cnat) :=
  fun (x: Q) (f: Q->Q) =>
    m Q (n Q x f) f.

Definition mul (Q: Prop) (n m: cnat) := 
  fun (x: Q) (f: Q->Q) =>
    m Q x (fun z => n Q z f).

(*
 *  Exercice 5
 *)
Definition cnat_t := forall Q: Type, Q->(Q->Q)->Q.
Definition zero_t (Q: Type) := fun (x: Q) (f: Q->Q) => x.
Definition succ_t (Q: Type) (n: cnat_t) := 
  fun (x: Q) (f: Q->Q) => 
    f (n Q x f).

Theorem no_succ : forall (Q: Type) (n: cnat_t), (n Q) <> succ_t Q n.
Proof.
  intros. intro.
  compute in H.
  f_equal

Theorem zero_not_one : forall (Q: Type) (x: Q) (f: Q->Q), zero_t Q <> succ_t Q zero_t.
Proof.
  intros.
  simpl.
  



  