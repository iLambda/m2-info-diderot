Require Import Omega.

Inductive bool : Type := true : bool | false : bool.
Definition negb := fun (x: bool) => match x with 
  | false => true
  | true => false
end.
Definition andb := fun (x y: bool) => match x, y with
  | true, true => true
  | _, _ => false
end.


Inductive even : nat -> Prop :=
    | even0 : even 0
    | evenS n : even n -> even (S (S n)).

Lemma even_is_double : forall n, even n -> exists m, n=m+m.
Proof.
  intros.
  induction H.
    * exists 0.
      auto.
    * destruct IHeven.
      exists (S x).
      firstorder.
Qed. 
     
(* Exercice 2 *)
Fixpoint belast (x: nat) (l: list nat) := match l with 
  | nil => nil
  | cons y l => cons x (belast y l)
end.

Lemma length_belast (x : nat) (s : list nat) : length (belast x s) = length s.
Proof.
  intros.
  induction s.
  * cbv. auto.
  * simpl. cbn. transitivity (S (length s)).  
    ** cbn. rewrite <- IHs.
       f_equal.
       assert (forall (u v: nat) (l: list nat), length(belast u l) = length (belast v l)).
         *** intros. induction l; cbn; auto. 
         *** apply H.
    ** auto.
Qed.   
    
Fixpoint skip (s: list nat) := match s with 
  | nil => nil
  | cons x nil => nil
  | cons x (cons y tail) => cons x (skip tail) 
end.
      
Lemma length_skip : forall l, 2 * length (skip l) <= length l.
Proof.
  intros.
  induction l.
    * cbn. auto.
    * case_eq l.
      ** intros. cbn. auto.
      ** intros. 
         cbn.
         assert (forall (x y: nat), x <= y <-> S x <= S y). intros. firstorder.
         apply -> H0.
         assert (forall (x: nat), S (x + 0) = S x). intros. auto.
         rewrite H1.
         assert (forall (x y z: nat), y + x <= z <-> x + y <= z). intros. firstorder.
         rewrite H2.
         rewrite Nat.add_succ_l.
         apply -> H0.
         cbn.
         assert (forall (x: nat), x + x = 2 * x). firstorder.
         rewrite H3.
         admit.
Admitted.
  

Fixpoint prodn (A: Type) (n: nat) := match n with 
  | 0 => 
  | 1 => A
  | S k => prod A (prodn A k)
end.
