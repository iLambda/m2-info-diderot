# Galois connections

Given two posets $(C, \leqslant)$ and $(A, \sqsubseteq)$, the pair $(\alpha : C \to A, \gamma : A \to C)$ is a **Galois connection** iff :
$$
\forall a \in A, c \in C \quad \alpha(c) \sqsubseteq a \iff c \leqslant \gamma(a)
$$

It is a pair of two functions (one that goes to the abstract to concrete, and the other way). $A$ is the abstract doman, and $\alpha$ is the abstraction (upper adjoint). $C$ is the concrete domain, and $\gamma$ is the concretization (lower adjoint). The property they satisfy means that if one element is lesser than some other element, then the realisation of this element is less than the initial abstraction of the element.

![](19-09-11_3.png)

It is noted :
$$
(C, \leqslant) \leftrightarrows^\gamma_\alpha (A, \sqsubseteq)
$$

## Properties

We have the following properties :
* $\gamma \circ \alpha$ is extensive, $\alpha \circ \gamma$ is reductive
* $\alpha$ is monotonic, $\gamma$ is monotonic
* $\gamma \circ \alpha \circ \gamma = \gamma$, $\alpha \circ \gamma \circ \alpha = \alpha$
* $\alpha \circ \gamma$ and $\gamma \circ \alpha$ are idempotent.

Also, $\alpha$ and $\gamma$ respect the meet and the join :
$$
\forall X \subseteq C \quad \vee X \text{ exists } \implies \alpha(\vee X) = \sqcup \{ \alpha(x) \mid x \in X \}\\
\forall X \subseteq A \quad \sqcap X \text{ exists } \implies \gamma(\sqcap X) = \wedge \{ \gamma(x) \mid x \in X \}
$$

### Alternate characterisation

If, for a given $(C, \leqslant), (A, \sqsubseteq)$, we have :
* $\gamma$ and $\alpha$ are monotonic

* $\gamma \circ \alpha$ is reductive

* $\alpha \circ \gamma$ is extensive

Then, $(C, \leqslant) \leftrightarrows^\gamma_\alpha (A, \sqsubseteq)$ is a galois connection.

Each adjoint can also be uniquely defined in term of each other :
* $a(c) = \sqcap \{a \mid c \leqslant \gamma(a)\}$
* $\gamma(a) = \vee \{c \mid \alpha(c) \sqsubseteq a\}$

### Deriving

One can derive different galois connections from existing ones :

* **duality** :
  $$
  (C, \leqslant) \leftrightarrows^\gamma_\alpha (A, \sqsubseteq) \iff (A, \sqsupseteq) \leftrightarrows_\gamma^\alpha (C, \geqslant)
  $$

* **point-wise lifting**
* **composition**

### Galois embeddings

The following properties are equivalent :
* $\alpha$ surjective
* $\gamma$ injective
* $a \circ \gamma = \operatorname{id}$

Such $(\alpha, \gamma)$ is called a **Galois embedding**, which is noted:
$$
(C, \leqslant) \substack{\leftarrow \\ \twoheadrightarrow}^\gamma_\alpha (A, \sqsubseteq)
$$

A galois connection can be made into an embedding by quotienting $A$ by $\mathcal R = \{ (a, a') \mid \gamma(a) = \gamma(a') \}$.

![](19-09-18_1.png)

## Approximations

### Operator approximations

One does not need to have a *Galois embedding* to do abstract interpretation.

Let us have $(C, \leqslant)$ a concrete poset, $(A, \sqsubseteq)$ an abstract poset, and a **monotonic concretization** $\gamma : A \to C$. We say :

* $a \in A$ is a **sound abstraction** of $c \in C$ if $c \leqslant \gamma(a)$

* $g : A \to A$ is a **sound abstraction** of $f : C \to C$ if $\forall a \in A , \ (f \circ \gamma)(a) \leqslant (\gamma \circ g)(a)$

* $g : A \to A$ is an **exact abstraction** of $f : C \to C$ if $\forall a \in A , \ f \circ \gamma \leqslant \gamma \circ g$

The upside of using a galois connection is that we always get the **best abstraction**.

If $g$ and $g'$ soundly abstract respectively $f$ and $f'$ :

* if $f$ is monotonic, $g \circ g'$ is a **sound abstraction** of $f \circ f'$

* if $g$, $g'$ are exact abstractions of $f$, $f'$, then $g \circ g'$ is an **exact abstraction** of $f \circ f'$

* if $g$, $g'$ are best abstractions of $f$, $f'$, then $g \circ g'$ is **not necessarily the best abstraction** of $f \circ f'$

### Fixpoint approximations

#### Strong approximation

> If we have :
> * $(C, \leqslant) \leftrightarrows^\gamma_\alpha (A, \sqsubseteq)$
>
> * **monotonic** concrete and abstract functions $f : C \to C$, $f^\# : A \to A$
> * $\alpha \circ f = f^\# \circ \alpha$
> * an element $a$ and its abstraction $a^\# = \alpha(a)$
> Then :
> $$
\alpha(\operatorname{lfp}_a f) = \operatorname{lfp}_{a^\#} f^\#
> $$

#### Weak approximation  

> If we have :
> * a **complete lattice** $(C, \leqslant, \vee, \wedge, \bot, \top)$
>
> * a **monotonic** concrete function $f$
> * a **sound abstraction** $f^\# : A \to A$ of $f$
> $$
> \forall x^\# : (f \circ \gamma)(x^\#) \leqslant (\gamma \circ f^\ast)(x^\#)
> $$
> * a **post-fixpoint** $a^\#$ of $f^\#$ (meaning $f^\#(a^\#) \sqsubseteq a^\#$)
>
> Then $a^\#$ is a sound abstraction of $\operatorname{lfp} f$, meaning :
>$$
\operatorname{lfp} f \leqslant \gamma(a^\#)
>$$

# Concrete semantics

Let's consider the following language :
```
stat ::= X ← exp                      (assign)
      | if exp <> 0 then stat         (condition)
      | while exp <> 0 do stat done   (loop)
      | stat; stat                    (sequence)

exp ::= X           (variable)
      | -exp        (negation)
      | exp o exp   (binary operation)
      | c           (constant)
      | [c, c']     (random input)
```

The variables are represented by the domain $\mathbb V$, and the set of values are represented by the domain $\mathbb Z$.

## State semantics

The semantics of expressions are denoted by :
$$
[\![ e ]\!] : (\mathbb V \to \mathbb Z) \to \mathcal P(\mathbb Z)
$$
The definition is recursive. It takes a memory state $\mathbb V \to \mathbb Z$, and outputs $\mathcal P(\mathbb Z)$.

Atomic commands are denoted by :
$$
C [\![\texttt{com}]\!] : \mathcal P(\mathcal E) \to \mathcal P(\mathcal E)
$$

Program execution can be seen as discrete **transitions** between states.
* $\Sigma$ : set of **states**
* $\tau \subseteq \Sigma \times \Sigma$ : a **transition relation**, written $\sigma \to \sigma'$

It is a form of **small-step semantics**.

The semantic of a program is now a set of **traces**, which are a *set of states*. They carry more information than states, and can prove more expressive properties :
* temporal properties

* computation length

* liveness

## State semantics and properties

### Forward semantics

The forward image is :
$$
\operatorname{post}_\tau(S) = \{ \sigma' \mid \exists \sigma \in S : \sigma \to \sigma \}
$$

It is a **complete $\cup$-morphism** in $(\mathcal P(\Sigma), \subseteq, \cup, \cap, \varnothing, \Sigma)$.

The **blocking states** are the $\mathcal B = \{\sigma \mid \forall \sigma' \in \Sigma : \sigma \not \to \sigma \}$, the states with no successors. It contains valid final states, but also errors.

The **reachable states** from $\mathcal I$ in the transition system is :
$$
\begin{align}
\mathcal R(\mathcal I) = \bigcup_{n \geqslant 0} \operatorname{post}^n_\tau (\mathcal I) &= \operatorname{lfp} F_\mathcal{R} \text{ with } F_\mathcal{R} = \mathcal I \cup \operatorname{post}_\tau(S)\\
&= \operatorname{lfp}_\mathcal{I} G_\mathcal{R} \text{ with } G_\mathcal{R} = S \cup \operatorname{post}_\tau(S)
\end{align}
$$

It can be expressed as a *fixpoint*. This can be proven by one application of Tarski's fixpoint theorem, and then a proof by recurrence over the number of states $n$ that $F_\mathcal{R}$ is the union of all reachable states after $n$ steps. The proof is similar for the second case, with $G_\mathcal{R}$.

![](19-09-18_4.png)

Using these notations, one can simply express the *absence of runtime error* in a given program : simply prove that $\mathcal R(\mathcal I) \cap \mathcal B \subseteq F$ (meaning that the only blocking reachable state is the end of the program).

To show an **invariant property**, find an inductive invariant $I \subseteq \Sigma$ the set of states. Verify that $\mathcal I \subseteq I$ (it contains the initial state), and $\forall \sigma \in I, \sigma \to \sigma' \implies \sigma' \in I$, meaning that the predicate is invariant to program transitions. This implies the desired property.

### Backward semantics

We define $\mathcal C(\mathcal F)$ the state **co-reachable** from $\mathcal F$ in the transition system :
$$
\begin{align}
\mathcal C(\mathcal F) &= \{ \sigma \mid \exists n \geqslant 0, \sigma_0 ... \sigma_n : \sigma = \sigma_0, \sigma_n \in \mathcal F, \forall i : \sigma_i \to \sigma_{i+1} \}\\
&= \bigcup_{n \geqslant 0} \operatorname{pre}^n_\tau(\mathcal F)
\end{align}
$$

![](19-09-18_3.png)

#### Sufficient precondition semantics

Let $\mathcal S(\mathcal Y)$ be :
$$
\begin{align}
\mathcal S(\mathcal Y) &= \{ \sigma \mid \forall n \geqslant 0, \sigma_0 ... \sigma_n : (\sigma = \sigma_0 \wedge \forall i : \sigma_i \to \sigma_{i+1} \implies \sigma_n \in \mathcal Y)\}\\
&= \bigcap_{n \geqslant 0} \tilde{\operatorname{pre}}^n_\tau(\mathcal Y) = \operatorname{gfp}(F_\mathcal{S}) \text{ where } F_\mathcal{S}(S) = \mathcal Y \cap \tilde{pre}_\tau(S)
\end{align}
$$
Where pre is the states such that all the successors satisfy $S$. It is a complete $\cap$-morphism.
$$
\tilde{\operatorname{pre}}_\tau(S) = \{ \sigma \mid \forall \sigma' : \sigma \to \sigma' \implies \sigma' \in S \}
$$

![](19-09-18_2.png)

This kind of semantics is useful for inferring **function contracts**, or **counter-examples**. It requires *under-approximations* to build decidable abstractions, but most techniques can only provide over-approximations.

## Finite trace semantics
