# Linear logic

The **provability problem** is the following : given an $LL$ formula $A$, do we have $\vdash A$ ?

## Provability & complexity

### Case of $LK$

In the case of classical logic, we have :
* unit-propositional logic: $O(n)$

* propositional logic: $\operatorname{co-NP}$ complete

* first-order : $\mathrm{undecidable}$

* second-order : $\mathrm{undecidable}$


### Case of $LL$

For linear logic, we have :

|                 | MLL                 | MALL                      | MELL        | LL          |
|-----------------|---------------------|---------------------------|-------------|-------------|
| LL prop         | $\operatorname{NP}$ | $\operatorname{PSPACE}$   | ?     | $\mathrm{undecidable}$ |
| LL first order  | $\operatorname{NP}$ | $\operatorname{NEXPTIME}$ | $\mathrm{undecidable}$ | $\mathrm{undecidable}$ |
| LL second order | $\mathrm{undecidable}$         |                           |             |             |

## Logical embeddings

There are multiple ways of encoding $LJ$ (intuitionistic logic) and $LK$ (classical logic) into $LL$. A reduction strategy (call by name or call by value) has to be chosen to describe accurately what will be done with the resources.

The arrow is translated using the following table :

|     | $LJ$                  | $LK$                   |
|-----|-----------------------|------------------------|
| cbn | $!A^n \multimap B^n$  | $!?A^t \multimap ?B^t$ |
| cbv | $!A^v \multimap !B^v$ | $!A^q \multimap ?!B^q$ |

### Embedding $LJ$ into $LL$

We will only consider the **call-by-name** translation of LJ.
The translation $^n$ is defined as follows :
$$
\begin{align*}
X^n &= X\\
(A \to B)^n &= \ !A^n \multimap B^n\\
(A \wedge B)^n &= A^n \& B^n\\
(A \vee B)^n &= \ !A^n \oplus !B^n\\
(\top)^n &= \top\\
(\bot)^n &= 0\\
(\Gamma \vdash A)^n &= \ !\Gamma^n \vdash A^n
\end{align*}
$$

The translation is **sound**, meaning :
$$
\Gamma \vdash_{LJ} A \implies \ !\Gamma^n \vdash_{LL} A^n
$$

It can be proven by structural induction on a proof tree.


### Embedding $LK$ into $LL$
