# $\Lambda$-calculus

## Equivalence

Let us define the notion of **head context** :
$$
H ::= \square \mid \lambda x. H \mid Ht \mid H[x/t]
$$

The equivalence is the following :
* the two contexts are alpha convertible alpha-convertible

* $H\langle t \rangle[x/u] \equiv H \langle t[x/u] \rangle$ iff $x \notin \mathrm{fv}(H)$ and no capture of free variables


Let us define the notion of **box context** :
$$
B ::= t \square \mid t [x / \square]
$$

We have :
$$
B[y / u] ::= t[y/u] \square \mid t[y/u][x/B]
$$

The reduction rules are :

* $L \langle \lambda x. t \rangle y \to_{dB} L \langle t [x/u] \rangle$

* $x [x/u] \to_{var} u$

* $t[x/u] \to_{gc} t$ if $x \notin \mathrm{fv}(t)$

* $B \langle \langle v \rangle \rangle [x / u] \to_{arg} B \langle \langle v[x/u] \rangle \rangle$ (the notation with double angles denotes the context where the hole has been replaced by $t$ without capture of free variables).

* $B \langle \langle v \rangle \rangle [x/u] \to_{dup} B[x/u] \langle \langle v \rangle \rangle [x / u]$ if $x \in \mathrm{fv}(v), x \in \mathrm{fv}(B)$

## Some properties

We have **stability of free variables**, meaning :

> 1. If $t \equiv u$, then $\mathrm{fv}(t) = \mathrm{fv}(u)$
>
> 2. If $t \to_{dB, var, gc, arg, dup} u$, then
> 3. If $t \to_{gc} u$, then $\mathrm{fv}(t) \supseteq \mathrm{fv}(u)$

## Translation to proof nets
