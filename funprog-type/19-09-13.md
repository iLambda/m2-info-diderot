# Functional programming and type systems

Functionnal programming allows us to write safer code. It also allows us to see our programs as objects, thanks to first order functions, and reason about them as mathematical objects.

Recursive functions can efficietly be compiled (case of terminal recursive functions), and they're usually easier to reason with in terms or formal proofs.

This course wishes to teach two things at the same time :
* key *programming techniques*
* the (meta)*theory of programming languages*

## Review of lambda-calculus

$\lambda$-calculus is the formal model that underlies all functional programming languages.

The abstract syntax is the following :
```
t, u ::= x | λx.t | tu
```

The reduction used on $\lambda$-terms is called $\beta$-reduction :
$$
(\lambda x.t) u \longrightarrow_\beta t[x/u]
$$

To model a programming language, more steps are needed :
* fix a **reduction strategy**

* develop **efficient execution mechanisms**

* **enrich the language** with primitive data types and recursion, GADTs, ...

* define a **static type system**

An **operational semantics** describes the *actions of a machine*, in the simplest possible manner (meaning, at the most abstract level).

## Program behavior

Sequences of reduction steps describe the behavior of a term. There are three mutually exclusive situations :
* **termination** : $t \to t_1 \to ... \to v$. The value $v$ is the result of evaluating $t$.

* **divergence** :  $t \to t_1 \to ... \to t_n \to ...$. The sequence of reductions is infinite. The term t *diverges*.

* **error** : $t \to t_1 \to ... \to t_n \not \to.$ where $t_n$ is not a value. It does not reduce, $t_n$ is stuck. In pure lambda calculus, there are *no closed stuck terms*. The language has to be enriched with more constructs, so there are cases where no reduction rules can be applied.

## Reduction strategies

We need deterministic evaluation strategies to accurately represent most programming languages.


### Evaluation contexts

We shall first define **head reductions**:
$$
\frac{}{(\lambda x.t) v \longrightarrow_{head\ cbv} t[v/x]} \qquad
$$

Evaluation contexts $E$ are described informally as "terms with a hole". For call-by-value, it is described by :
```
E ::= [] | Eu | vE
```

Then, define reduction as *head reduction* under an evaluation context.

$$
\frac{t \longrightarrow_{head\ cbv} t'}{E[t] \longrightarrow_{head\ cbv} E[t']}
$$

### Different strategies

Values form a subset of terms :
```
v ::= x | λx.t
```

A value represents the **result** of a computation.

#### Call-by-value strategy

The **call-by-value** reduction relation $t \longrightarrow_{cbv} t'$ is inductively defined :
$$
\frac{}{(\lambda x.t) v \longrightarrow_{cbv} t[v/x]} \qquad
\frac{t \longrightarrow_{cbv} t'}{tu \longrightarrow_{cbv} t'u} \qquad
\frac{u \longrightarrow_{cbv} u'}{vu \longrightarrow_{cbv} vu'}
$$

This is known as a **small-step** operational semantics.

We will now detail the different features of call-by-value reduction.

##### Weak reduction

One cannot reduct under a lambda abstraction, meaning : **values do not reduce**. Also, we are only interested in reducing **closed terms** (terms with no free variables).

##### Strict evaluation

An actual argument is reduced to a value **before** it's passed to a function.

#### Call-by-name strategy

The **call-by-name** reduction relation $t \longrightarrow_{cbn} t'$ is inductively defined :
$$
\frac{}{(\lambda x.t) u \longrightarrow_{cbv} t[u/x]} \qquad
\frac{t \longrightarrow_{cbv} t'}{tu \longrightarrow_{cbv} t'u}
$$

The unevaluated argument is passed to the funciton. It is reduced every time the function demands the value, if it ever happens.

#### Call-by-need strategy

This strategy eliminates the main efficiency of CBN, which is repeated computation. This is done by uzing **memoization**. It is used in haskell.

#### Comparison in between strategies

If $t$ terminates under CBV, it terminates under CBN. The converse is false (for instance, $(\lambda x. 1)\omega$).

In fact, the **standardization theorem** implies that if $t$ can be reduced to a value, via any strategy, then it can be reduced to a value via CBN.

### Encoding reduction strategies

#### CBN in CBV

In order to envode CBN strategy into CBV, can be done using **thunks** (suspended computations, here implemented as ̀`unit -> 'a` functions).

$$
\begin{align}
[ x ] &= x \ ()\\
[\lambda x.t] &= \lambda x. [t]\\
[t u] &= [t] (\lambda _ .[u])
\end{align}
$$

To prove the transformation is correct (**simulation theorem**), you would have to prove that :
$$
t \longrightarrow_{cbn} u \implies [t] \longrightarrow_{cbv} [u]
$$

A similar theorem could be proved in the same way for types.

# Towards machine-checked proofs

Formalizing programming languages can be useful, to make formal proofs over the behavior of the program and obtain precise definitions.

Machines can help us to formalize such large proofs of programs, with the help of certified tools like CompCERT or Astrée. Plus, hand-written proofs are seldom trustworthy, nor easy to make evolve.

## Syntax with binders

### Binding and $\alpha$-equivalence

Most programming languages provide constructs to **bind** variables. For instance, local definition (`let x = t in t'`), or quantifiers ($\forall a. \ a \to a$).

$\alpha$-equivalence is a relation that allows renaming of bound variables. The informal idea is to say that two programs where one variable can be safely renamed without changing the meaning of the program are equivalent (for instance, $\lambda x. x$ and $\lambda y. y$). Formally, it is defined as :
$$
\lambda x.t \equiv_\alpha \lambda y. t[y/x] \quad \text{ if } y \notin \operatorname{fv}(\lambda x.t)
$$
