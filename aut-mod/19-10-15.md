# Probabilistic automata

## Formalism

A matrix $M \in \mathbb R^{p \times q}$ is **stochastic** iff :
* All of its entries are in $[0, 1]$
  $$
  \forall i, j \quad M_{ij} \in [0, 1]
  $$

* the sum of the numbers on each row are $1$
  $$
  \forall p \quad \sum_q M_{pq} = 1
  $$

A **probabilistic automaton** is a tuple $\mathscr A = (\Sigma, Q, S, \mu, T)$ where :

* $\Sigma$ is a finite *alphabet*

* $Q$ the finite set of *states*

* $S \in [0, 1]^{1 \times Q}$ is a stochastic row vector of *initial probabilities*

* $T \in [0, 1]^{Q \times 1}$ is a vector of *accepting states*, where $T_i = 1$ iff $i$ is final, else $0$

* $\mu(a) \in [0, 1]^{Q \times Q}$ is a stochastic matrix of *transition probabilities*, for every $a \in A$

For every word $w \in A$, define $\mu(w)$ as :
$$
\mu(w) = \mu(w_1) \cdot ... \cdot \mu(w_{|w|})
$$

We define the **probability of acceptance** of a word $w$ as :
$$
\mathbb P_{\mathscr A}(w) = S \mu(w) T \in \mathbb R
$$
It is also equal to the sum of the probability of the paths that lead to an accepting state.

### Stochastic matrices

We have the following closure property :

> If $M, N$ are stochastic matrices, then $MN$ is stochastic.

### Threshold language

For any probability automata $\mathscr A$, and threshold $\lambda \in [0, 1]$, define :
$$
\mathscr L_A(\lambda) = \{ w \in A^\ast \mid \mathbb P_{\mathscr A}(w) > \lambda\}
$$

More generally, if you let $\bowtie \in \{ >, <, \geqslant, \leqslant \}$ :
$$
\mathscr L_A^\bowtie(\lambda) = \{ w \in A^\ast \mid \mathbb P_{\mathscr A}(w) \bowtie \lambda\}
$$

### Myhill-Nerode

Let $L \subseteq A^\ast$ be a language. Two word $u$ and $v$ are right equivalent for $L$ ($u \equiv_L v$) iff :
$$
\forall w \in A^\ast \quad uw \in L \iff vw \in L
$$

We have :

> $L$ regular $\iff$ the number of equivalence classes of $\equiv_L$ are finite

In fact, the number of equivalent classes in the relation is the number of states of any minimal DFA for $L$.

## Regularity

We have :

> For every regular language $L$, there exists a probability automaton $\mathscr A$ such that :
>$$
\mathscr L_{\mathscr A} (\lambda) = L
>$$
>For all $\lambda \in [0, 1)$

### Non-regular languages

We have :

> There exists a probabilistic automaton $\mathscr A$ and a threshold $\lambda$ such that $\mathscr L_{\mathscr A}$ is not regular

The idea is that we can encode almost everything in the probability of the word, which allows us to informally dispose of unbounded memory. Let $\mathscr A = (A, Q, S, \mu, T)$, with :
* $A = \{0, 1\}$
* $Q = \{p, q\}$
* $S = [1, 0]$

The automata is :

![](19-10-15_1.png)

Now, let for each word $w$, let $[w]$ be :
$$
[w] = \sum_{i = 1}^{|w|} w_i 2^{i - |w| - 1} = \overline{0.w_n ... w_1}^2
$$

We claim that, for every $w$ :
$$
S \mu(w) = [1 - [w] \quad [w]]
$$

Thus, if $\lambda < \lambda'$, there exists a word separating the two thresholds, because $[A^\ast]$ is dense in $[0, 1]$. As such, for every $\lambda \neq \mu$ :
$$
\mathscr L_{\mathscr A}(\lambda) \neq \mathscr L_{\mathscr \mu}(\mu)
$$

There is uncountably many languages for a given threshold, so by a generalized pigeonhole argument, we know that there must be a non-regular language.

In fact, $\mathscr L_{\mathscr A}(\lambda)$ is regular iff $\lambda$ is rational.

Some of the languages can even be uncomputable, for instance by using an uncomputable threshold.

We will, in the following, resctrict ourselves to rational thresholds.

### Universally non-regular automaton

>There exists a probabilistic automata $\mathscr A$ such that $\mathscr L_{\mathscr A}(\lambda)$ is not regular, for all $\lambda \in (0, 1)$

The automata is :

![](19-10-15_2.png)

We have :
$$
\begin{align*}
\mathbb{P}_{\mathscr B}(u \sharp v) &=
\mathbb{P}_{\mathscr B}(p \to^u q) \times \mathbb{P}_{\mathscr B}(q \to^\sharp p) \times \mathbb{P}_{\mathscr B}(p \to^v q)\\
&= \mathbb{P}_{\mathscr B}(u) \times \mathbb{P}_{\mathscr B}(v)\\
&= [u] \cdot [v]
\end{align*}
$$

Let $\lambda \in (0, 1)$. By density, take $u$, $v$ such that $\lambda < [u] < [v] < 1$. By density, there exists $w$ such that :
$$
\frac{\lambda}{[v]} < [w] <
\frac{\lambda}{[u]}
$$

Hence, $\mathbb P_{\mathscr B}(u \sharp w) = [u][w] < \lambda$. But, $\mathbb P_{\mathscr B}(vw) = [v][w] > \lambda$. Hence, $u \sharp v \notin L$, but $v \sharp w \in L$. This shows that :
$$
u \not\equiv_L v
$$
Since there are infinitely many pairs $u, v$, then the language cannot be regular by Myhill-Nerode.

## Isolated cut-points

All the previous automata satisfy :
$$
\forall \varepsilon > 0, \mathscr {L}_{\mathscr A}(\lambda) \neq \mathscr {L}_{\mathscr A}(\lambda + \varepsilon)
$$

We say a threshold (or **cut-point**) $\lambda$ is **isolated**, if there exists $\delta > 0$ such that :
$$
|\mathbb P_{\mathscr A}(w - \lambda)| \geq \delta \quad \forall w \in A^*
$$

This especially implies that :
$$
\forall \varepsilon < \delta, \mathscr {L}_{\mathscr A}(\lambda) = \mathscr {L}_{\mathscr A}(\lambda + \varepsilon)
$$

### Regularity

Let $\lambda$ be an isolated cut-point of isolation threshold $\delta$.

We have that :
> $\mathscr L_{\mathscr A}(\lambda)$ is regular.
> If $\mathscr A$ has $n$ states and $r$ final states, then $\mathscr L_{\mathscr A}(\lambda)$ can be recognized by a DFA that has at most $\left(1 + \frac{r}{d}\right)^{n-1}$

## Operations

One can build an automata $\mathscr C$ from $\mathscr A$ and $\mathscr B$ such taht :

* $\mathbb P_{\mathscr C} = \mathbb P_{\mathscr A} \cdot \mathbb P_{\mathscr B}$

* $\mathbb P_{\mathscr C} = \alpha \mathbb P_{\mathscr A} + (1 - \alpha)\mathbb P_{\mathscr B}$
