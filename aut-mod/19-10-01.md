# B-regular expressions

A **B-regular expression** is given by the syntax :
$$
E ::= \varnothing \mid \varepsilon \mid a \in A \mid E + E \mid E \cdot E \mid E^\ast \mid E^B
$$

As for regular expressions, the language recognized by an expression $E$ can be defined recursively, and is noted $\mathscr L(E)$. The $E^B$ can be seen as a "repeat up to $n$ times" operator.

The denotational semantic for B-regular expressions is the following
$$
\begin{align*}
{[\![E]\!]}_B : A^\ast &\longrightarrow \mathbb N \cup \{\infty\}\\
u &\longmapsto \inf \{ n \mid u \in \mathscr L(E[L^{\leqslant n} \setminus L^B]) \}
\end{align*}
$$

Let's give some examples.
$$
\begin{align*}
{[\![a^B]\!]}_B(u) &= \abs u_a \text{ if $u \in a^\star$, else } \infty\\
{[\![b^\ast(ab^\ast)^B]\!]}_B(u) &= \abs u_a\\
{[\![b^\ast(ab^\ast)^B + a^\ast(ba^\ast)^B]\!]}_B(u) &= \min (\abs u_a, \abs u_b)\\
{[\![(a^\ast b)^\ast a^B (ba^\ast)^\ast]\!]}_B(u) &= \operatorname{min\_block}(u)\\
{[\![(a^Bb)^\ast a^B]\!]}_B(u) &= \operatorname{max\_block}(u)
\end{align*}
$$

With the function $\operatorname{min\_block}$ defined as the minimum of $n_0, ..., n_l$ in the word $a^{n_0}ba^{n_1}b...ba^{n_l}$.

## Results & theorems

> If $E$ is a regular expression, then :
> $$
{[\![E]\!]}_B(u) =
\left\{
\begin{align*}
  0 &\text{ if } u \in \mathscr L(E)\\
  \infty &\text{ otherwise}
\end{align*}
\right.
= \Large \chi_{\small \mathscr L(E)}
> $$

We have the following important theorem :
> B-regular expressions $\approx$ B-automata

### Equivalence between B-automata and B-regexp

#### $\impliedby$

By induction on $E$, we define an automata for B-regexps.

| B-regexp | Computed function | Automata |
|:-------------:|:------------------------------------------------------------------------------------------------------:|:--------------------------------------------------:|
| $\varnothing$ | $u \mapsto \infty$ | $\varnothing$ |
| $\varepsilon$ | $u \mapsto 0$ if $\varepsilon$, $\infty$ otherwise | ![](19-10-01_1.png) |
| $a$ |  | ![](19-10-01_2.png) |
| $K + L$ | $\min([\![K]\!], [\![L]\!])$ | Done by disjoint union of automata for $K$ and $L$ |
| $K \cdot L$ | $u \mapsto \inf \{ n \mid u = vw, u \in \mathscr L(K[\leqslant n]), w \in \mathscr(K[\leqslant n]) \}$ | ![](19-10-01_3.png) |
| $K^\ast$ |  | ![](19-10-01_4.png) |
| $K^B$ | $u \mapsto \inf_{u = u_1 ... u_k} \max(k, max [\![K]\!](u_i))$ | ![](19-10-01_5.png) |

#### $\implies$

Let $E_{a,p,q}$ be the words accepted by a run from $p$ to $q$.
