# Lower bound for Grover's algorithm

The framework we'll work with is the following :
* **input** : $f : \{0, 1\}^n \to \{0, 1\}$

* **access** to $O_f(\ket x_X \ket b_B) = \ket x_X \ket{b \oplus f(x)}_B$

* **goal** : find $x$ such that $f(x) = 1$
