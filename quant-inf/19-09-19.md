# CHSH game

There are **2 players**.
* $A$ has bit $x$ given as input
* $B$ has bit $y$ given as input

The probability of having a bit $\in \{0, 1\}$. is uniform, independant.

The **goal** is :
* $A$ outputs $a \in \{0, 1\}$
* $B$ outputs $b \in \{0, 1\}$

Such that : $a \oplus b = w \wedge y$.

## Winning strategies

### Classical strategy

The best classical strategy has a win probability of $\frac{3}{4}$. This strategy corresponds to : always output $0$.

| $x \backslash y$ | $0$ | $1$  |
|----------|-----|------|
| $0$      | Win | Win  |
| $1$      | Win | Lose |

### Quantum strategy

There exists a quantum strategy that allows to win with probability $85\%$.

The state $|\phi\rangle = \frac{1}{\sqrt 2}(|00\rangle + |11\rangle)$ is an entangled state, which is shared between $A$ and $B$.

Then, the strategy consists in :
* for $A$ :
  * if $x = 0$, $A$ measures $|\phi\rangle$ in the standard basis tilted by $\frac{-\pi}{6}$
  * if $x = 1$, $A$ measures $|\phi\rangle$ in the standard basis tilted by $\frac{3\pi}{6}$
* for $B$ :
  * if $y = 0$, $B$ measures $|\phi\rangle$ in the standard basis tilted by $\frac{-\pi}{6}$
  * if $y = 1$, $B$ measures $|\phi\rangle$ in the standard basis tilted by $\frac{3\pi}{6}$

After rotation, the state is :
$$
\frac{1}{\sqrt 2}[\cos(\theta_A + \theta_B)(|00\rangle - |11\rangle) + \sin(\theta_A + \theta_B)(|01\rangle - |10\rangle)]
$$  
We obtain a probability of :

| $x \backslash y$ | $0$ | $1$  |
|----------|-----|------|
| $0$      | $-\frac{\pi}{8}$ | $\frac{\pi}{8}$  |
| $1$      | $\frac{\pi}{8}$ | $\frac{3\pi}{8}$ |


The probability of winning is :
$$
\left(\cos\left(\frac{\pi}{8}\right)\right)^2 = 0.85
$$

# Quantum computation

## Quantum evolution

When you want to modify a classical system, you apply a boolean function to change a number of bits. Given that quantum systems are probabilistic, the "state modifiers" of quantum systems are going to be modeled by something similar to stochastic matrices.

Since we want to preserve $L_2$-norm, we're going to use **unitary matrices**.

### Unitary matrices

A matrix $U$ is said to be **unitary** iff :
$$
U \cdot U^\dagger = U^\dagger \cdot U = \operatorname{Id}
$$

It **preserves the inner product**, meaning :
$$
\langle u|v \rangle = \langle Uu | Uv \rangle
$$
Which implies preservation of the $L_2$-norm :
$$
|| v || = ||Uv||
$$
Also, the rows (or columns) of $U$ form an *orthonormal basis*.

For instance, the **Hadamard matrix** is such an operator :
$$
H= \frac{1}{\sqrt 2}
  \left( {\begin{array}{cc}
   1 & 1 \\
   1 & -1 \\
  \end{array} } \right)
$$
It is another way of writing :
$$
\left\{
\begin{align}
  H|0\rangle &= \frac{1}{\sqrt 2} (|0\rangle + |1\rangle)\\
  H|1\rangle &= \frac{1}{\sqrt 2} (|0\rangle - |1\rangle)
\end{align}
\right.
$$

## Quantum circuit model

### Gates

In all the following :
$$
U^b =
\left\{
\begin{align}
I &\quad \text{ if } b = 0\\
U &\quad \text{ if } b = 1
\end{align}
\right.
$$

#### Hadamard $\left(H\right)$

$$
H = \frac{1}{\sqrt 2}
  \left( {\begin{array}{cc}
   1 & 1 \\
   1 & -1 \\
  \end{array} } \right)
$$

![](19-09-19_2.png)

$$
H|\varphi\rangle = |\psi\rangle
$$

#### NOT gate $\left(X\right)$

$$
X =
  \left( {\begin{array}{cc}
   0 & 1 \\
   1 & 0 \\
  \end{array} } \right)
$$

![](19-09-19_3.png)

$$
X|0\rangle = |1\rangle\\
X|1\rangle = |0\rangle
$$

#### CNOT gate

$$ \operatorname{CNOT} =
  \left( {\begin{array}{cc}
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 0 & 1\\
    0 & 0 & 1 & 0\\
  \end{array} } \right)
$$

![](19-09-19_1.png)

$$
|a, b\rangle \mapsto |a, a \oplus b\rangle
$$


#### Phase flip $\left(Z\right)$

$$
Z =
  \left( {\begin{array}{cc}
   1 & 0 \\
   0 & -1 \\
  \end{array} } \right)
$$

To state it another way :

$$
Z|0\rangle = |0\rangle\\
Z|1\rangle = -|1\rangle
$$

#### Toffoli gate

![](19-09-19_7.png)

$$
|a, b, c\rangle \mapsto |a, b, c \oplus (a \wedge b)\rangle
$$

### Inverse circuit

Running a circuit in reverse reverses the mapping, due to the unitarity of quantum states.

## Superdense coding

**Superdense coding** is a quantum communication protocol to transmit two classical bits of information, by sending only one qubit, under the assumption that both parties are pre-sharing an entangled state (an EPR pair).

Let the following states :
$$
\begin{align}
\vert \varphi^+ \rangle &= \frac{1}{\sqrt 2} (|00\rangle + |11\rangle)\\
\vert \varphi^- \rangle &= \frac{1}{\sqrt 2} (|00\rangle - |11\rangle)\\
\vert \psi^+ \rangle &= \frac{1}{\sqrt 2} (|01\rangle + |10\rangle)\\
\vert \psi^- \rangle &= \frac{1}{\sqrt 2} (|01\rangle - |10\rangle)\\
\end{align}
$$

And consider the following circuit :

![](19-09-19_4.png)

We claim that, for this circuit :
$$
\begin{align}
\vert 00 \rangle &\mapsto |\varphi^+\rangle\\
\vert 01 \rangle &\mapsto |\varphi^-\rangle\\
\vert 10 \rangle &\mapsto |\psi^+\rangle\\
\vert 11 \rangle &\mapsto |\psi^-\rangle\\
\end{align}
$$

The protocol for superdense coding is the following :

![](19-09-19_5.png)

## Quantum teleportation

The goal of **quantum teleportation** is to send 1 qubit $|\psi\rangle$ by using 2 classical bits and 1 shared EPR-pair $| \varphi^+\rangle$.

![](19-09-19_6.png)

If $A$ measures $m_0 m_1$, $B$ has the state :
$$
\alpha |m_2\rangle + (-1)^{m_1} |m_2 \oplus 1\rangle
$$

To decode it, it suffices to apply $X^{m_2} z^{m_1}$.

## Quantum complexity

We say a quantum circuit (family) $\mathscr C = \{\mathscr C_n\}_{n \geqslant 0}$ **computes a (boolean) function** (family)
$$
f_n : \{0, 1\}^n \mapsto \{0, 1\}
$$
if it implements the following unitary
$$
\forall x \in \{0, 1\}^n, b \in \{0, 1\}, z \in \{0, 1\}^n \quad U |x\rangle |z\rangle |b\rangle = |x\rangle |z\rangle |b \oplus f(x)\rangle
$$

The **size** of the circuit $\mathscr C$ is :
$$
s(n) = \text{ size of the circuit computing } f_n
$$

### Classical complexity classes

The class $\operatorname{P}$ is the set of languages computable with a classical circuit family, uniform, of size polynomial in $n$.

The class $\operatorname{BPP}$ is the set of languages computable with bounded error in probabilistic polynomial time. Let $L \in \operatorname{BPP}$. Then, there exists a circuit family of polynomial size circuits that compute the language, such that :
$$
\begin{align}
x \in L &\implies \mathbb P_{r \in \{0, 1 \}^{\operatorname{poly}}} ((x, r) \in L') \geqslant \frac{2}{3}\\
x \notin L &\implies \mathbb P_{r \in \{0, 1 \}^{\operatorname{poly}}} ((x, r) \in L') \leqslant \frac{1}{3}
\end{align}
$$


Similarly, $\operatorname{BQP}$ is class  of languages computable by a polynomail-size *uniform* quantum circuit family with probability $\geqslant \frac{2}{3}$ of being correct.

#### $\operatorname{BPP} \subseteq \operatorname{BQP}$

We claim :
$$
\operatorname{BPP} \subseteq \operatorname{BQP}
$$

Let $\mathcal C = {(\mathcal C_n)}_{n \geqslant 0}$ be a circuit family for $L'$, with inputs $x$ and $r$, and such that :
$$
\begin{align}
1 &\text{ if } x \in L \text{ with high probability}\\
0 &\text{ if } x \notin L \text{ with high probability}
\end{align}
$$

We will simulate $\mathcal C_n$ using the **Toffoli gate**. Any classical state (say $\neg$ and $\wedge$) can be simulated by a Toffoli gate, and some auxiliary constant bits $0$, $1$.

![](19-09-19_8.jpg)

To recover $x$ and $n$, we uncompute by using the transpose of the system.

![](19-09-19_9.jpg)
