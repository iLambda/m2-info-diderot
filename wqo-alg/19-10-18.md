# Lossy counters

[TODO]

## Problems

### Accessibility

For LCM, the problem of **accessibility** is Ackermann-hard in terms of complexity.

#### Primitive recursive functions

Let ${(F_k)}_{k \in \mathbb N} : \mathbb N \to \mathbb N$, such that :
* $F_0(n) = n+1$

* $F_{k+1}(n) = F_k^{n+1}(n)$ (here, the exponent means iterated composition)

Each $F_k$ is primitive recursive. Forall $f$ primitive recursive, there exists $k$ such that $f \leqslant F_k$.

Let $F_\omega(n) = F_n(n)$. This function is not $f$ primitive recursive, by contraposition of the previous statement. We will call $\mathrm{Ack}(n)$ the function $F_\omega(n)$.

Let's try to rewrite this function without using recursion. Let $F(\vec a, n)$ with $\vec a = (a_0, ..., a_{m-1}) \in \mathbb N^m$. For a fixed $n$ :
* $F(\vec 0, n) = n+1$

* $F(\overrightarrow{a_0 + 1, ..., a_{m-1}}, n) = F(a_0, a_1, ..., n+1)$

* $F(\overrightarrow{0, ..., 0, a_i + 1, ..., a_{m-1}}, n) = F(0, ..., n+1, a_i, ..., a_{m-1})$
