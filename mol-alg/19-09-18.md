# Approximation algorithms

## Chernoff Bound

Let $\chi_1, ..., \chi_n$ be $\{0,1 \}$ random variables with probability $p_i$ to take the value of $1$. Furthermore, $\chi_i$s are mutually independant.
$$
E\left(\sum \chi_i \right) = \mu \leqslant U \implies \mathbb P\left( \sum X_i \geqslant (1 + \delta)U \right) \leqslant \left( \frac{e^\delta}{(1+\delta)^{1 + \delta}} \right)^U
$$

## Other examples of approximation problems

### Traveling salesman problem

Given $G = (V, E)$ and a metric cost $c : E \mapsto \mathbb R^+$, find a "tour" $R$, such that $\sum_{e \in R} c(e)$ that is minimized. The cost function must satisfy the triangle inequality :
$$
\forall x, y, z \quad c(x, y) + c(y, z) \geqslant c(x, z)
$$

> **NB** : the graph here has to be complete, for a cost to be metric over $G$


#### $2$-approximation

```
Build a min-cost spanning tree T
Doubling the edges in T, to create T²
Build an eulerian tour according to T²
Build a real tour R by skipping vertices already visited
```

It is possible to compute an eulerian tour because we doubled the edges ; each vertex has an even degree in the double graph.


The optimal tour is a cycle. By removing an edge, we obtain a spanning tree, and we know it has to be $\geqslant$ in cost compared to the min-spanning tree.

$$
\operatorname{OPT} \geqslant c(T)
$$

We also notice that because $c$ is metric, the cost of the solution is less than two times the cost of the spanning tree.
$$
c(R) \leqslant 2 \cdot c(T) \leqslant 2 \operatorname{OPT}
$$
$\square$

#### Christofides algorithm

```
Build a min-cost spanning tree T
Let V⁻ denote the set of odd-degree nodes in T
Find a min-cost perfect matching M
Build an eulerian tour according to M
Build a tour R accordingly
```

We have that :
$$
\operatorname{OPT} \leqslant 2 \cdot c(M)
$$

From that, and $\operatorname{OPT} \geqslant c(T)$, we deduce :
$$
c(R) \leqslant c(M) + c(T) \leqslant \frac{3}{2} \operatorname{OPT}
$$

### Integer multi-commodity flow

Given $G = (V, E)$ with pairs of terminals $(s_1, t_1), ..., (s_k, t_k) \in V$. Find paths $p_i$ for $s_i$ to $t_i$ forall $i$ such that the **congestion** $c$ is minimized :
$$
c = \max_{e \in E} |\{P_i \mid P_i \ni e\}|
$$

#### Linear programming relaxation

Let's state the program as a linear programming problem.
$$
\min w\\
\sum_{p \in \mathbb P_i} \chi_i = 1 \quad \forall i
\sum_{i = 1}^k \sum_{\substack{p \in \mathbb P_i\\e \ni p}} \chi_p \leqslant w \quad \forall e \in E\\
\forall p \in \bigcup^k_{i = 1}\mathbb P_i
$$

Consider the following algorithm :
```
Solve the LP
Let χ*, w* be the OPT fractional solution
∀i, choose path p ∈ Pi with probability χ*_p
Voilà
```

If $w^* \geqslant 1$, then this algorithm is an $O\left( \frac{\log n}{\log \log n} \right)$-approximation.

In expectation, the congestion on edge $e$ is :
$$
E\left(  \sum^k_{i = 1} \sum_{\substack{p \in \mathbb P_i\\p \ni e}}\chi^\ast_p \right) \leqslant w^*
$$

By applying the chernoff bound, we get :
$$
\begin{align}
\mathbb P \left(\text{congestion} \geqslant \underbrace{\frac{c \log n}{\log \log n}}_{(1 + \delta)} w^* \right) \leqslant \left(\frac{e^{ 1+ \delta}}{(1 + \delta)^{1 + \delta}}\right) \leqslant \left( \frac{1}{\left(\frac{c \log n}{e \log \log n}\right)^{\frac{c \log n}{\log \log n}}} \right)
\end{align}
$$

By union bound, with high probability $\geqslant 1 - \frac{1}{n}$, the congestion is :
$$
\frac{6e \log n}{\log \log n} w^* \leqslant \frac{6e \log n}{\log \log n} \operatorname{OPT}
$$

### Independent set

Given $G = (V, E)$, choose maximum size $U \subseteq V$ so that $E(U, U) = \varnothing$.

![](19-09-18_1.png)

#### The limits of linear programming

Let's state the problem as a linear problem.
$$
\min \sum_{v \in V} \chi_v\\
\chi_u + \chi_v \leqslant 1 \quad \forall e = (u, v)\\
\chi_v \geqslant 0 \quad \forall v \in V
$$

In this case, the fractional solution of the linear problem is totally useless, because the bound given by the relaxation is not good.

### $3$-coloring $\Delta$-dense graph

A graph $G = (V, E)$ is $\Delta$-dense for $0 \leqslant \Delta \leqslant 1$ if every node $u \in V$ has at least $\Delta \cdot n$ neighbors.

Consider the following algorithm :
```
Pick a subset S ⊆ V by including each vertex independently with probability 10log(n)/Δn
Try out every possible coloring in S
  For each coloring guess, solve the 2-SAT problem based on the guess
```

We have :
$$
E(|S|) = \frac{10 \log n}{\Delta}
$$
Let's show :
$$
\mathbb P\left(|S| > \frac{20 \log n}{\Delta}\right) \leqslant \frac{1}{n^{\Omega(1)}}
$$
Indeed :
$$
\begin{align}
\mathbb P\left(|S| > \frac{20 \log n}{\Delta}\right) &=
\mathbb P\left(|S| > (1+\delta)E(|S|)\right) \quad \text{ with } \delta = 1 \\
&= \mathbb P(|S| > (1 + \Delta) E(|S|)) = \left(\frac{e}{2^2}\right)^{\frac{10\log n}{\Delta}}  \text{ by the Chernoff bound}\\
&= \left(\frac{e^5}{2^{10}}\right)^{\frac{2\log n}{\Delta}} \leqslant \left( \frac{1}{e} \right)^{\frac{2 \log n}{\Delta}} \leqslant \frac{1}{n^{\frac{2}{\Delta}}}
\end{align}
$$

We state :
> $\forall v \in V$ with P(none of its neighbours is in S) ⩽ 1/n^Ω(1)

Let's assume $u$ has $k \geqslant \Delta n$ neighbors. Then, $\mathbb P( \text{none of its neighbours is in S} ) = 1 - \left(\frac{10 \log n}{\Delta}\right)^k$.
$$
\begin{align}
1 - \left(\frac{10 \log n}{\Delta}\right)^k &\leqslant 1 - \left(\frac{10 \log n}{\Delta}\right)^{\Delta n} \leqslant \frac{1}{n^{10}}
\end{align}
$$

With $\geqslant 1 - \frac{1}{n}$, the set $S$ has size $\leqslant \frac{20 \log n}{\Delta}$, and every node has at least one neighbor in $S$.

The total number of colorings is $3^{|S|} \leqslant 3^{\frac{20 \log n}{\Delta}} = n^{O(20/\Delta)}$. Thus, bruteforcing them can be done in polynomial time.
